<?php

namespace App\Http\Controllers\Home;

use App\Models\Article;
use App\Models\Comment;
use App\Models\Links;
use App\Models\Tags;
use function request;
use function view;

class ArticleController extends CommonController
{
    public function details()
    {
        $cate_id = request('cate_id', 0);
        $article_id = request('article_id', 0);
        // 获取文章数据信息
        $article_model = new Article();
        $top_article = $article_model->articleList($cate_id);
        $hot_article = $article_model->articleList(-1);
        $article_data = $article_model->getDetails($article_id);
        // 获取友情链接信息
        $links_model = new Links();
        $links_list = $links_model->getList();
        // 获取标签信息
        $tags_model = new Tags();
        $tags_list = $tags_model->getList([['looks', '>', 0]]);
        // 获取评论数据
        $comment_model = new Comment();
        $comment_list = $comment_model->commentList($article_id);

        return view('home/details', compact([
            'article_data', 'top_article', 'hot_article',
            'links_list', 'tags_list', 'cate_id', 'comment_list'
        ]));
    }

    public function comment()
    {

    }

}
