<?php

namespace App\Http\Controllers\Home;


use function view;

class IndexController extends CommonController
{
    public function __construct()
    {
        $this->getArticle = true;
        parent::__construct();
    }

    public function index()
    {
        // 获取广告数据
        return view('home/index');
    }

    public function category()
    {
        return view('home/article');
    }

    public function tags()
    {
        return view('home/article');
    }

}
