<?php
/**
 * Created by PhpStorm.
 * Author: sitenv@aliyun.com
 * CreateTime: 2023/2/11 17:49
 * Blog：www.myblogs.xyz
 */

namespace App\Http\Controllers\Api;

use app\models\Demo;
use app\services\CommonServices;

class DemoController extends CommonController
{
    /**
     * 案例1
     */
    public function domeOne()
    {
        // 设定必传参数
        $this->require = ['mobile', 'password'];

        // 验证参数
        if (!$this->verifyParam()) return $this->verifyError();

        // 业务处理
        /*......*/

        // 返回结果
        // return $this->jsonError('requestFailed');

        // 返回结果
        return $this->jsonSuccess('requestSuccess');
    }

    /**
     * 案例2
     */
    public function domeTwo()
    {
        // 设置必传参数
        $this->require = ['mobile', 'password'];

        // 设置非必传参数
        $this->selectable = ['verify_code'];

        // 验证参数
        if (!$this->verifyParam()) return $this->verifyError();

        // 业务处理
        /*......*/

        // 返回结果
        // return $this->jsonError('requestFailed');

        // 返回结果
        return $this->jsonSuccess('requestSuccess');
    }

    /**
     * 案例3
     */
    public function domeTwoOne()
    {
        // 设置全部参数非必传
        $this->selectable = ['mobile', 'password', 'verify_code', 'reserved'];

        // 验证参数
        if (!$this->verifyParam()) return $this->verifyError();

        // 业务处理
        /*......*/

        // 返回结果
        // return $this->jsonError('requestFailed');

        // 返回结果
        return $this->jsonSuccess('requestSuccess');
    }

    /**
     * 高级案例1：保存数据
     */
    public function highRegisterDome()
    {
        // 设置全部参数非必传
        $this->require = ['mobile', 'password', 'verify_code', 'reserved'];

        // 验证参数
        if (!$this->verifyParam()) return $this->verifyError();

        // 业务处理
        /*......*/
        // 获取模型
        $domeModel = new Demo();
        // 保存更新
        if (!$domeModel->operation($this->data)){
            // 返回结果
            return $this->jsonError($domeModel->getError());
        }
        // 返回结果
        return $this->jsonSuccess('requestSuccess');
    }

    /**
     * 高级案例2：获取数据
     */
    public function highFindDome()
    {
        // 设置全部参数非必传
        $this->require = ['mobile'];

        // 验证参数
        if (!$this->verifyParam()) return $this->verifyError();

        // 业务处理
        /*......*/
        // 获取模型
        $domeModel = new Demo();
        // 获取数据（单条）
        $result = $domeModel->getFind($this->data);
        // 获取数据（多条）
        $result = $domeModel->getList($this->data);
        // 获取数据（多条分页）
        $result = $domeModel->getListPage($this->data, 15);

        // 返回结果
        // return $this->jsonError('requestFailed');

        // 返回结果
        // return $this->jsonSuccess();
        return $this->jsonResult($result);
    }

    /**
     * 高级案例3：获取数据
     */
    public function highFindsDome()
    {
        // 设置全部参数非必传
        $this->require = ['mobile'];

        // 验证参数
        if (!$this->verifyParam()) return $this->verifyError();

        // 业务处理
        /*......*/
        // 获取模型
        $domeModel = new Demo();

        // 查询条件参数设置方式（1）参数较多时推荐使用，或者根据业务需求和在模型中设置默认值
        $domeModel->where = '';
        $domeModel->fields = '';
        $domeModel->orders = '';
        $domeModel->whereOr = '';
        // 获取数据（单条）
        $result = $domeModel->getFind($this->data);
        // 获取数据（多条）
        $result = $domeModel->getList($this->data);
        // 获取数据（多条分页）
        $result = $domeModel->getList($this->data, 15);

        // 查询条件参数设置方式（2）
        $dome_where = $this->data;
        $dome_fields = '';
        $dome_orders = '';
        $dome_whereOr = '';
        // 获取数据（单条）
        $result = $domeModel->getFind($dome_where, $dome_fields);
        // 获取数据（多条）
        $result = $domeModel->getList($dome_where, 15, $dome_orders, $dome_fields, $dome_whereOr);
        // 获取数据（多条分页）
        $result = $domeModel->getListPage($dome_where, 15, $dome_orders, $dome_fields, $dome_whereOr);

        // 返回结果
        // return $this->jsonError('requestFailed');

        // 返回结果
        return $this->jsonSuccess($result);
        // $this->jsonSuccess($result, '请求成功');
    }

    /**
     * 其它项自行查看，如批量删除
     * $this->domeRemove($where);
     */
    public function domeRemove()
    {
        // 设置全部参数非必传
        $this->require = ['id'];
        // 验证参数
        if (!$this->verifyParam()) return $this->verifyError();
        // 业务处理
        if (CommonServices::delete(new Demo(), $this->data['id'])){
            return $this->jsonError('deleteFailed');
        }
        return $this->jsonSuccess();
    }
}