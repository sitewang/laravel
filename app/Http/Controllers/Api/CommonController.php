<?php

namespace App\Http\Controllers\Api;

use App\Constants\ResponseCode;
use Illuminate\Routing\Controller;

class CommonController extends Controller
{
    protected $require = [];
    protected $selectable = [];
    protected $data = [];
    protected $userId = 0;
    protected $userData;
    protected $httpUrl = '';

    public function __construct()
    {
        // 读取站点配置信息
    }

    /**
     * 验证请求参数
     * @param array $param
     * @param string $request
     * @return bool
     */
    protected function verifyParam($param = [], $request = 'post')
    {
        if ($request == 'post'){
            $post_data = request()->post();
        }else{
            $post_data = request()->all();
        }
        if (!empty($param)){
            $this->require = $param;
        }
        if (!empty($this->require)){
            if (is_string($this->require)){
                $this->require = explode(',', $this->require);
            }
        }
        if (!empty($this->selectable)){
            if (is_string($this->selectable)){
                $this->selectable = explode(',', $this->selectable);
            }
        }
        if (!empty($this->require) || !empty($this->selectable)){
            if (empty($this->selectable)){
                $param = $this->require;
            }else{
                $param = array_merge($this->require, $this->selectable);
            }
            $array_keys = array_keys($post_data);
            foreach ($array_keys as $key) {
                if (!in_array($key, $param)){
                    unset($post_data[$key]);
                }
            }
            if (!empty($this->require)){
                foreach ($this->require as $val){
                    if (!isset($post_data[$val]) || $post_data[$val] === ''){
                        $this->error = '缺少请求参数：' . $val;
                        $this->code = ResponseCode::PARAM_ERR;
                        return false;
                    }
                }
            }
            $this->data = $post_data;
            // 是否保存记录参数信息
            if (env('APP_DEBUG')){
                $this->consoleLog($this->data);
            }
        }
        return true;
    }

    /**
     * 验证请求头
     * @param $require
     * @return bool
     */
    protected function verifyToken($require = true)
    {
        $auth_token = request()->header('Authorization');
        if (empty($auth_token)){
            if ($require){
                $this->error = '缺少请求头Authorization';
                $this->code = ResponseCode::UNAUTH;
                return false;
            }
        }
        $user_token = new UserToken();
        $result = $user_token->tokenAuth(md5($auth_token));
        if (empty($result->user_id)){
            if ($require){
                $this->error = '系统检测到您长时间未登陆，请重新登录';
                $this->code = ResponseCode::UNAUTH;
                return false;
            }
        }else{
            // 获取用户信息
            $user_model = new User();
            $user_fields = 'id,phone,openid,invite_id,status,third_check';
            $user_data = $user_model->getFind($result->user_id, $user_fields);
            if (empty($user_data)){
                if ($require) {
                    $this->error = '系统检测到您长时间未登陆，请重新登录';
                    $this->code = ResponseCode::UNAUTH;
                    return false;
                }
            }else{
                $this->data['user_id'] = $user_data->id;
                $this->userId = $user_data->id;
                $this->userData = $user_data;
            }
        }
        return true;
    }

    protected function verifyError()
    {
        return $this->jsonError($this->error, $this->code);
    }

    protected function getInfo()
    {
        if (!$this->verifyParam('content')) return $this->verifyError();
        $post_data = $this->data['content'];
        $delimiter = chr("123") . chr("123");
        $result = explode($delimiter, $post_data);
        if (count($result) != 3 && count($result) != 2){
            $this->error = '请求失败';
            $this->code = ResponseCode::PARAM_ERR;
            return false;
        }
        if (count($result) == 2){
            $mobile = ($result['0'] - 321) / 123;
            $this->data = ['account'=>$mobile,'type'=>$result['1']];
        }else{
            $this->error = '请求失败';
            $this->code = ResponseCode::PARAM_ERR;
            return false;
        }
        return true;
    }

    /**
     * 附加参数
     * @param int $code
     * @param array $data
     * @param string $message
     */
    protected function jsonReturn($code = 0, $data = [], $message = '')
    {
        $result = [
            'msg'  => $message,
            'code' => $code,
            'data' => $data,
        ];
        return response()->json($result);
    }

    /**
     * 请求验证失败
     * code 9999：请求验证失败
     * @param string $message
     * @param array $data
     */
    protected function jsonFail($message = '', $data = [])
    {
        return $this->jsonReturn(ResponseCode::FORBIDDEN, (object)$data, $message);
    }

    /**
     * 操作数据成功
     * code 1000：操作数据成功；可能有数据返回对象形式
     * @param string $message
     * @param array $data
     */
    protected function jsonSuccess($message = '', $data = [])
    {
        return $this->jsonReturn(ResponseCode::SUCCESS, (object)$data, $message);
    }

    /**
     * 操作数据失败
     * code 1001：操作数据失败
     * @param string $message
     * @param array $data
     */
    protected function jsonError($message = '', $data = [])
    {
        return $this->jsonReturn(ResponseCode::PARAM_ERR, (object)$data, $message);
    }

    /**
     * 获取数据成功
     * code 1000：获取数据成功；肯定有数据返回对象数组形式
     * @param array $data
     * @param string $message
     */
    protected function jsonResult($data = [], $message = '')
    {
        return $this->jsonReturn(ResponseCode::SUCCESS, $data, $message);
    }

    /**
     * 没有更多数据
     * code 1001：没有更多数据；空对象数组数据
     * @param array $data
     * @param string $message
     */
    protected function jsonEmpty($message = '', $data = [])
    {
        return $this->jsonReturn(ResponseCode::SUCCESS, $data, $message);
    }

    /**
     * TOKEN失效
     * code 1002：TOKEN失效
     * @param array $data
     * @param string $message
     */
    protected function jsonTokenFailed($message = '', $data = [])
    {
        return $this->jsonReturn(ResponseCode::UNAUTH, $data, $message);
    }

    /**
     * 数据日志保存
     * @param $data
     * @param $type
     * @param int $flags
     */
    protected function consoleLog($data, $type = '', $flags = FILE_APPEND)
    {
        $dir = getcwd() . '/api_log/';
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
        $html = '';
        $filename = $dir . $type . date('YmdH') . '.html';
        if (!file_exists($filename)){
            $html .= '<!DOCTYPE html><html><head>';
            $html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
            $html .= '</head><body>';
        }
        $html .= '<div style="display: inline-block;margin-left: 35%">';
        $html .= '<h5 style="margin: 5px 0">---------------------------------------------------------------</h5>';
        $html .= '<h5 style="margin: 5px 0">执行日期：' . date('Y-m-d H:i:s', time()) . '</h5>';
        $html .= '<h5 style="margin: 5px 0">请求地址：<textarea style="display: inline-block;width: 100%;border: none;font-size: 14px;font-weight: bold;font-family: bold;">' . $this->httpUrl . request()->url() . '</textarea></h5>';
        $html .= '<h5 style="margin: 5px 0">数据信息：<textarea style="display: inline-block;width: 100%;border: none;font-size: 14px;font-weight: bold;font-family: bold;">' . json_encode($data) . '</textarea></h5>';
        $html .= '</div>';
        if (!file_exists($filename)){
            $html .= '</body></html>';
        }
        file_put_contents($filename , $html, $flags);
    }
}