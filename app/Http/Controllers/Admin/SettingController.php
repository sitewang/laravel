<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/6 5:49 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends CommonController
{
    // 站点设置
    public function site()
    {
        $setting_model = new Setting();
        $site = $setting_model->settingFind('site');
        return view('admin/site_setting', compact('site'));
    }

    public function save(Request $request)
    {
        $data = $request->post();
        if (!empty($data)){
            unset($data['_token']);
            $setting_model = new Setting();
            if (!$setting_model->insetSetting('site', $data)){
                $this->returnError('保存失败');
            }
            $this->returnSuccess('保存成功');
        }
    }

}
