<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/6 5:18 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Http\Controllers\Admin;

use App\Models\Notice;
use App\Services\CommonService;
use Illuminate\Http\Request;

class NoticeController extends CommonController
{
    public function index()
    {
        $notice_model = new Notice();
        $notice_model->fields = ['contents,update_time', true];
        $notice_list = $notice_model->getLists('', 10);
        return view('admin/notice', compact('notice_list'));
    }

    public function insert()
    {
        $this->getData();
        return view('admin/save_notice');
    }

    public function update()
    {
        $this->getData();
        return view('admin/save_notice');
    }

    protected function getData()
    {
        $notice_id = request('notice_id', 0);
        $oldData = [];
        if (!empty($notice_id)){
            $notice_model = new Notice();
            $oldData = $notice_model->getFind($notice_id);
        }
        view()->share('oldData', $oldData);
    }

    public function save(Request $request)
    {
        $data = $request->post();
        if (!empty($data)){
            $notice_model = new Notice();
            unset($data['_token']);;
            if (empty($data[$notice_model->getPrimaryKey()])){
                $data['create_time'] = NEW_TIME;
            }else{
                $data['update_time'] = NEW_TIME;
            }
            $rows = $notice_model->operation($data);
            if (!$rows){
                $this->returnError($notice_model->getError());
            }
            $this->returnSuccess('保存成功', url('admin/notice'));
        }
    }

    public function delete()
    {
        if (CommonService::delete(new Notice())){
            $this->returnSuccess('删除成功');
        }
        $this->returnError('删除失败');
    }

}
