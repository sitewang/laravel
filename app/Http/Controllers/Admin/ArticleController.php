<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/6 4:39 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\Category;
use App\Models\Tags;
use App\Services\CommonService;
use App\Services\CommonServices;
use Illuminate\Http\Request;

class ArticleController extends CommonController
{
    public function index()
    {
        $article_model = new Article();
        $article_list = $article_model->articleList('', '', true, 10);
        return view('admin/article', compact('article_list'));
    }

    public function insert()
    {
        $this->getData();
        return view('admin/save_article');
    }

    public function update()
    {
        $this->getData();
        return view('admin/save_article');
    }

    protected function getData()
    {
        // 获取分类信息
        $category_model = new Category();
        $category_list = $category_model->getList();
        $article_id = request('article_id', 0);
        $oldData = [];
        if (!empty($article_id)){
            $article_model = new Article();
            $oldData = $article_model->getFind($article_id);
            if (!empty($oldData)){
                $oldData->tags_name = '';
                if (!empty($oldData->tags)){
                    $tags_model = new Tags();
                    $oldData->tags_name = $tags_model->getTagsName($oldData->tags);
                }
                $oldData->contents = empty($oldData->contents) ? '' : htmlspecialchars_decode($oldData->contents);
            }
        }
        if (!empty($category_list)){
            foreach ($category_list as $key=>&$val){
                $val->is_check = 0;
                if (!empty($oldData) && !empty($oldData->cate_id)){
                    if ($oldData->cate_id == $val->category_id){
                        $val->is_check = 1;
                    }
                }else{
                    if ($key == 0){
                        $val->is_check = 1;
                    }
                }
            }
        }

        view()->share([
            'category_list'=>$category_list,
            'oldData'=>$oldData
        ]);
    }

    public function save(Request $request)
    {
        $data = $request->post();
        if (!empty($data)){
            $article_model = new Article();
            $tags_model = new Tags();
            unset($data['_token']);
            $data['tags'] = $tags_model->getTagsId($data['tags']);
            $data['contents'] = empty($data['contents']) ? '' : htmlspecialchars($data['contents']);
            $data['user_id'] = 1;
            if (empty($data[$article_model->getPrimaryKey()])){
                $data['create_time'] = NEW_TIME;
            }else{
                $data['update_time'] = NEW_TIME;
            }
            $rows = $article_model->operation($data);
            if (!$rows){
                $this->returnError($article_model->getError());
            }
            $this->returnSuccess('保存成功', url('admin/article'));
        }
    }

    public function delete()
    {
        if (CommonServices::delete(new Article())){
            $this->returnSuccess('删除成功', url('admin/article'));
        }
        $this->returnError('删除失败');
    }

}
