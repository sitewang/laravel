<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/5 11:14 AM
 * Blog：www.myblogs.xyz
 */

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;

class CommonController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $userData = [];
    protected $userId = 0;

    public function __construct()
    {
        // 检测账户是否登录
        $user_id = session('user_id');
        $user_id = empty($user_id) ? 0 : intval($user_id);
        if (empty($user_id)){
            redirect('admin/login')->send();
        }
        $user_model = new User();
        $userData = $user_model->getFind($user_id);
        if (empty($userData)){
            redirect('admin/login')->send();
        }
        $this->userId = $userData->user_id;
        $this->userData = $userData;
        session('user_id', $this->userId);
        // 获取站点信息
        $setting_model = new Setting();
        $site = $setting_model->settingFind('site');

        // 获取当前路由信息
        $routeControllerName = '';
        $route = request()->route();
        if ($route) {
            $action = request()->route()->getAction();
            $controller = '@';
            if (isset($action['controller'])) {
                $controller = class_basename($action['controller']);
            }
            list($routeControllerName, $routeActionName) = explode('@', $controller);
        }

        view()->share([
            'site'=>$site,
            'userId'=>$this->userId,
            'nickname'=>$userData->nickname,
            'routeControllerName'=>$routeControllerName,
        ]);

    }

    /**
     * @param $message
     * @param bool $close
     * @param int $time
     */
    protected function returnError($message, $close = false, $time = 3000)
    {
        $str = '<script>';
        $str .= 'parent.error("' . $message . '",' . $time . ',' . $close . ');';
        $str .= '</script>';
        exit($str);
    }

    /**
     * @param $message
     * @param string $jumpUrl
     * @param bool $close
     * @param int $time
     */
    protected function returnSuccess($message, $jumpUrl = '', $close = 'off', $time = 3000)
    {
        $str = '<script>';
        $str .= 'parent.success("' . $message . '",' . $time . ',\'jumpUrl("' . $jumpUrl . '","' . $close . '")\');';
        $str .= '</script>';
        exit($str);
    }

    /**
     * @param int $status 返回状态
     * @param string $data 返回成功数据
     * @param string $message 返回信息
     * @return string
     */
    protected function jsonReturn($status = 0, $data = '', $message = '')
    {
        header('Content-Type:application/json; charset=utf-8');
        $jsonData = [
            'status'   => $status,
            'data'     => $data,
            'message'  => $message
        ];
        exit(json_encode($jsonData));
    }
}
