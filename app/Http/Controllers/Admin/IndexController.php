<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/5 11:21 AM
 * Blog：www.myblogs.xyz
 */

namespace App\Http\Controllers\Admin;


use App\Models\Article;
use App\Models\Comment;
use App\Models\Links;
use App\Models\User;
use App\Services\CommonService;
use Illuminate\Support\Facades\DB;
use function view;

class IndexController extends CommonController
{
    public function index()
    {
        $count_info = new \stdClass();
        $count_info->article_count = CommonService::getCount(new Article());
        $count_info->comment_count = CommonService::getCount(new Comment());
        $count_info->links_count = CommonService::getCount(new Links());
        $count_info->user_count = CommonService::getCount(new User());
        // 系统信息
        $val = 'version()';
        $mysql_version = DB::select('select version()');
        $system = new \stdClass();
        $system->project_version = 'MyBlogs v 1.0';
        $system->server = $_SERVER['SERVER_SOFTWARE'];
        $system->mysql = empty($mysql_version['0']) ? '未知' : $mysql_version['0']->$val;
        $system->up_max = ini_get('upload_max_filesize');
        $system->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $system->server_ip = $_SERVER['SERVER_NAME'].' [ '.gethostbyname($_SERVER['SERVER_NAME']).' ]';
        $system->php_mod = php_sapi_name();
        $system->php_os = PHP_OS;
        $system->coding = 'UTF-8';
        $system->time = date('Y年m月d日 H:s:i',time());
        $system->php_version = PHP_VERSION;

        return view('admin/index', compact([
            'count_info', 'system'
        ]));
    }

}
