<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/6 5:44 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Http\Controllers\Admin;

use App\Models\Links;
use App\Services\CommonService;
use Illuminate\Http\Request;

class LinksController extends CommonController
{
    public function index()
    {
        $links_model = new Links();
        $links_list = $links_model->getLists('', 10);
        return view('admin/links', compact('links_list'));
    }

    public function insert()
    {
        $this->getData();
        return view('admin/save_links');
    }

    public function update()
    {
        $this->getData();
        return view('admin/save_links');
    }

    protected function getData()
    {
        $links_id = request('links_id', 0);
        $oldData = [];
        if (!empty($links_id)){
            $links_model = new Links();
            $oldData = $links_model->getFind($links_id);
        }
        view()->share('oldData', $oldData);
    }

    public function save(Request $request)
    {
        $data = $request->post();
        if (!empty($data)){
            $links_model = new Links();
            unset($data['_token']);;
            if (empty($data[$links_model->getPrimaryKey()])){
                $data['create_time'] = NEW_TIME;
            }else{
                $data['update_time'] = NEW_TIME;
            }
            $rows = $links_model->operation($data);
            if (!$rows){
                $this->returnError($links_model->getError());
            }
            $this->returnSuccess('保存成功', url('admin/notice'));
        }
    }

    public function delete()
    {
        if (CommonService::delete(new Links())){
            $this->returnSuccess('删除成功');
        }
        $this->returnError('删除失败');
    }
}
