<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/6 5:33 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\Comment;
use App\Services\CommonService;

class CommentController extends CommonController
{
    public function index()
    {
        $comment_model = new Comment();
        $comment_list = $comment_model->getLists('', 10, 'create_time desc');
        return view('admin/comment', compact('comment_list'));
    }

    public function delete()
    {
        if (CommonService::delete(new Comment())){
            $this->returnSuccess('删除成功');
        }
        $this->returnError('删除失败');
    }

}
