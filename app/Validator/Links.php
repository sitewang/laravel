<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/10 7:15 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Validator;

class Links extends Common
{
    protected $rule = [
        'title'=>'required',
        'url'=>'required',
        'imgurl'=>'required'
    ];

    protected $messages = [
        'title.required'=>'请填写网站名称',
        'url.required'=>'请填写网站地址',
        'imgurl.required'=>'请填写网站LOGO地址'
    ];
}
