<?php

namespace App\Validator;


class User extends Common
{
    protected $rule = [
        'nickname' => 'required',
        'username' => 'required'
    ];

    protected $messages = [
        'username.required' => '请输入用户名',
        'nickname.required' => '请输入昵称'
    ];
}
