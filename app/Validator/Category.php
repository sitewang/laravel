<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/10 6:39 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Validator;

class Category extends Common
{
    protected $rule = [
        'name'=>'required',
        'alias'=>'required',
    ];

    protected $messages = [
        'name.required'=>'请填写栏目名称',
        'alias.required'=>'请填写栏目别名',
    ];
}
