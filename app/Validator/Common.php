<?php

namespace App\Validator;

use Illuminate\Support\Facades\Validator;

class Common extends Validator
{
    // 验证规则
    protected $rule = [];
    // 规则说明
    protected $messages = [];
    // 提示信息
    protected $errorMsg = '';

    public function check(array $data)
    {
        if (empty($data)) return false;
        $validator = self::make($data, $this->rule, $this->messages);
        $errors = $validator->errors();
        if (!empty($errors->all())){
            $this->errorMsg = $errors->first();
            return false;
        }
        return true;
    }

    public function getError()
    {
        return $this->errorMsg;
    }
}
