<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/10 5:50 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Validator;

class Notice extends Common
{
    protected $rule = [
        'title'=>'required',
        'contents'=>'required'
    ];

    protected $messages = [
        'title.required'=>'请填写文章标题',
        'contents.required'=>'请填写文章内容'
    ];
}
