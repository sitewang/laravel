<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/9 12:04 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Validator;

class Article extends Common
{
    protected $rule = [
        'title'=>'required',
        'contents'=>'required',
        'cate_id'=>'required'
    ];

    protected $messages = [
        'title.required'=>'请填写文章标题',
        'contents.required'=>'请填写文章内容',
        'cate_id.required'=>'请选择分类'
    ];

}
