<?php

namespace App\Models;


class Comment extends Common
{
    protected $table = 'my_comment';
    protected $primaryKey = 'comment_id';

    public function commentList($article_id)
    {
        $this->asJoin = ['c'=>['my_user as u', 'c.user_id', '=', 'u.user_id']];
        $this->fields = 'c.*,u.nickname,u.face';
        $this->timeRules = 'Y-m-d';
        $list_data = $this->getLists(['article_id'=>$article_id], 10);
        return $list_data;
    }

}
