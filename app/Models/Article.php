<?php

namespace App\Models;


class Article extends Common
{
    protected $table = 'my_article';
    protected $primaryKey = 'article_id';

    public function articleList($cate_id = 0, $tags_id = 0, $page = false, $limit = 5)
    {
        // 分页 $limit 每页多少数量
        $list_where = [];
        if (!empty($cate_id)){
            $list_where['cate_id'] = $cate_id;
        }
        if (!empty($tags_id)){
            $this->whereRaw = "FIND_IN_SET('$tags_id', tags)";
        }
        $orders = '';
        if (!$page){
            $this->asJoin = [];
            $orders .= 'looks desc,create_time desc';
            $this->fields = 'article_id,title,looks,text_number,create_time';
        }else{
            // 设置别名后相同字段需要加上筛选项
            $this->asJoin = ['a'=>[
                ['my_user as u', 'a.user_id', '=', 'u.user_id'],
                ['my_category as c', 'a.cate_id', '=', 'c.category_id']
            ]];
            $orders .= 'a.create_time desc';
            $this->fields = 'article_id,title,contents,text_number,a.create_time,';
            $this->fields .= 'tags,cate_id,a.user_id,is_top,u.nickname,c.name as cate_name';
        }
        $this->orders = $orders;
        if (!$page){
            $list_data = $this->getList($list_where, $limit);
        }else{
            $list_data = $this->getLists($list_where, $limit);
        }
        if (!empty($list_data->list)){
            if (is_object($list_data->list)){
                $comment_model = new Comment();
                $tags_model = new Tags();
                foreach($list_data->list as &$val){
                    $val->comment_count = $comment_model->getCount(['article_id'=>$val->article_id]);
                    $tags_array = $tags_model->getList([['tags_id', 'in', $val->tags]], '', '', 'name');
                    $tags_name = empty($tags_array) ? '' : implode('、', array_removal($tags_array));
                    $val->tags_name = $tags_name;
                    if (!empty($val->contents)){
                        $val->contents = htmlspecialchars_decode($val->contents);
                    }
                    if (!$page){
                        $val->create_time = date('Y年m月d日', $val->create_time);
                    }
                }
            }
        }
        return $list_data;
    }

    public function getDetails($article_id)
    {
        // 设置别名后相同字段需要加上筛选项
        $this->asJoin = ['a'=>[
            ['my_user as u', 'a.user_id', '=', 'u.user_id'],
            ['my_category as c', 'a.cate_id', '=', 'c.category_id']
        ]];
        $this->fields = 'article_id,title,source,contents,looks,text_number,a.create_time,';
        $this->fields .= 'tags,cate_id,a.user_id,is_top,u.nickname,c.name as cate_name';
        $data = $this->getFind($article_id);
        if (!empty($data)){
            $comment_model = new Comment();
            $data->comment_count = $comment_model->getCount(['article_id'=>$article_id]);
            $data->contents = htmlspecialchars_decode($data->contents);
            $data->create_time = date('Y年m月d日', $data->create_time);
            $tags_model = new Tags();
            $tags_array = $tags_model->getList([['tags_id', 'in', $data->tags]], '', '', 'name');
            $tags_name = empty($tags_array) ? '' : implode('、', array_removal($tags_array));
            $data->tags_name = $tags_name;
        }
        return $data;
    }

}
