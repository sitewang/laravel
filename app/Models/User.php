<?php

namespace App\Models;


use Illuminate\Support\Facades\DB;

class User extends Common
{
    protected $table = 'my_user';
    protected $primaryKey = 'user_id';
    protected $readers = [
        '金牌读者','银牌读者','铜牌读者','普通读者'
    ];

    public function getAccount()
    {
        $account = DB::table($this->table)->orderBy('account', 'desc')->value('account');
        if (empty($account)) return 10000;
        return $account + 1;
    }

    public function userList()
    {
        $fileds = 'user_id,username,nickname,face';
        $data = $this->getList(['status'=>0], '', '', $fileds);
        if (!empty($data)){
            $comment_model = new Comment();
            if (is_object($data)){
                foreach($data as $key=>&$val){
                    $val->readers = $this->readers[$key];
                    $comment_count = $comment_model->getCount(['user_id'=>$val->user_id]);
                    $val->comment_count = $comment_count;
                }
            }
        }
        return $data;
    }

    public function userLists($where = '', $limit = '', $order = '', $field = '', $whereOr = '')
    {
        $user_list = $this->getLists($where, $limit, $order, $field, $whereOr);
        if (!empty($user_list->list)){
            $article_model = new Article();
            foreach ($user_list->list as &$val){
                $val->article_count = $article_model->getCount(['user_id'=>$val->user_id]);
                $val->last_time = empty($val->last_time) ? '' : date('Y-m-d H:i:s', $val->last_time);
            }
        }
        return $user_list;
    }

}
