<?php

namespace App\Models;


class Auth extends Common
{
    public function __construct($name, $pk = '', $prefix = '')
    {
        parent::__construct();
        if ($pk !== false){
            $this->primaryKey = empty($pk) ? $name . '_id' : $pk;
        }
        $prefix = empty($prefix) ? env('DB_PREFIX') : $prefix;
        $this->table = $prefix . $name;
    }
}