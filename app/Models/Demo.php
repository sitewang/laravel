<?php
/**
 * Created by PhpStorm.
 * Author: sitenv@aliyun.com
 * CreateTime: 2023/2/11 17:50
 * Blog：www.myblogs.xyz
 */

namespace App\Models;

class Demo extends Common
{
    protected $table = 'demo';
    protected $primaryKey = 'id';
}