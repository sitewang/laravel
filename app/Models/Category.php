<?php

namespace App\Models;


class Category extends Common
{
    protected $table = 'my_category';
    protected $primaryKey = 'category_id';
}
