<?php

namespace App\Models;


class Tags extends Common
{
    protected $table = 'my_tags';
    protected $primaryKey = 'tags_id';

    public function getTagsId($tags = '')
    {
        if (empty($tags)) return '';
        $tags_array = explode('|', $tags);
        $tags_ids = [];
        if (!empty($tags_array)){
            if (is_array($tags_array)){
                foreach ($tags_array as $val){
                    $tags_id = $this->getField(['name'=>$val], 'tags_id');
                    if (empty($tags_id)){
                        $tags_id = $this->operation(['name'=>$val], false, false, true);
                    }
                    $tags_ids[] = $tags_id;
                }
            }
        }
        return empty($tags_ids) ? '' : implode(',', $tags_ids);
    }

    public function getTagsName($tagsIds = '')
    {
        if (empty($tagsIds)) return '';
        $tags_array = $this->getList([['tags_id', 'in', $tagsIds]]);
        $tags_name = [];
        if (!empty($tags_array)){
            foreach ($tags_array as $val){
                $tags_name[] = $val->name;
            }
        }
        return empty($tags_name) ? '' : implode('|', $tags_name);
    }

}
