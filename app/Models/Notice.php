<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/10 5:24 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Models;

class Notice extends Common
{
    protected $table = 'my_notice';
    protected $primaryKey = 'notice_id';
    public $orders = 'create_time desc';
}
