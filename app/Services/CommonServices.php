<?php
/**
 * Created by PhpStorm.
 * Author: sitewang@aliyun.com
 * CreateTime: 2022/4/7 2:47 PM
 * Blog：www.myblogs.xyz
 */

namespace App\Services;

class CommonServices
{
    public static function getCount($model, $where = '')
    {
        return $model->getCount($where);
    }

    public static function audit($field = 'status', $value = 0 , $default = 1, $model = '')
    {
        $param = request()->all();
        if (!empty($param)) {
            if (empty($model_id)){
                if (empty($param[$model->getPrimaryKey()])){
                    return false;
                }
                $model_id = $param[$model->getPrimaryKey()];
            }
            $rows = $model->status($model_id, $field, $value, $default);
            if (!$rows){
                return false;
            }
            return true;
        }
        return false;
    }

    public static function delete($model = '', $model_id = 0)
    {
        $param = request()->all();
        if (!empty($param)) {
            if (empty($model_id)){
                if (empty($param[$model->getPrimaryKey()])){
                    return false;
                }
                $model_id = $param[$model->getPrimaryKey()];
            }
            $rows = $model->remove($model_id);
            if (!$rows){
                return false;
            }
            return true;
        }
        return false;
    }

}
