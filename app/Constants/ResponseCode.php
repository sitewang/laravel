<?php
/**
 * Created by PhpStorm.
 * Author: sitenv@aliyun.com
 * CreateTime: 2022/7/17 11:34
 * Blog：www.myblogs.xyz
 */

namespace App\Constants;

class ResponseCode
{
    // 请求成功
    const SUCCESS = 1000;
    // 请求失败
    const ERROR = 1001;
    // 用户未授权
    const UNAUTH = 1002;
    // 拒绝访问
    const FORBIDDEN = 4405;
    // 未找到相关资源
    const NOT_FOUND = 4404;
    // 客户端请求参数错误
    const PARAM_ERR = 4422;
    // 服务器内部错误
    const SERVER_ERR = 5000;
}