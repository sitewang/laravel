// 操作
$(document).on("click", "a[lay-event='act']", function (e) {
    e.preventDefault();
    let obj = $(this);
    layer.confirm('您确定要' + obj.text() + '吗？', {
        btn: ['确定','取消'] //按钮
    }, function(index){
        $("#hide_form").attr('src', obj.attr('data-url'));
        setTimeout(function () {
            refresh();
            layer.close(index);
        }, 1000);
    });
});
$(document).on("click", "button[lay-event='act']", function (e) {
    e.preventDefault();
    let obj = $(this);
    layer.confirm('您确定要' + obj.text() + '吗？', {
        btn: ['确定','取消'] //按钮
    }, function(index){
        $("#hide_form").attr('src', obj.attr('data-url'));
        setTimeout(function () {
            refresh();
            layer.close(index);
        }, 1000);
    });
});
// 批量操作
$(document).on('click', "a[lay-event='list']", function (e) {
    e.preventDefault();
    let obj = $(this);
    layer.confirm('您确定要' + obj.html(), {
        btn: ['确定','取消'] //按钮
    }, function(index){
        obj.parents('form').attr('action', obj.attr('data-url')).submit();
        layer.close(index);
    });
});
$(document).on('click', "button[lay-event='list']", function (e) {
    e.preventDefault();
    let obj = $(this);
    layer.confirm('您确定要' + obj.html(), {
        btn: ['确定','取消'] //按钮
    }, function(index){
        obj.parents('form').attr('action', obj.attr('data-url')).submit();
        layer.close(index);
    });
});
// 新窗口打开
$(document).on("click", "a[lay-event='load']", function (e) {
    e.preventDefault();
    let obj = $(this);
    page(obj.text(), obj.attr('data-url'), obj.attr('w'),  obj.attr('h'));
});
$(document).on("click", "button[lay-event='load']", function (e) {
    e.preventDefault();
    let obj = $(this);
    page(obj.text(), obj.attr('data-url'), obj.attr('w'),  obj.attr('h'));
});

let index;

function page(title, url, w, h) {
    if(title == null || title === '') {
        title = false;
    }
    if(url == null || url === '') {
        url = "404.html";
    }
    if (w === undefined && h === undefined){
        index = layer.open({
            type: 2,
            title: title,
            shadeClose: true,
            fixed: false, //不固定
            area: ['100%', '100vh'],
            content: url,
        });
        layer.full(index);
    }else{
        index = layer.open({
            type: 2,
            title: title,
            shadeClose: true,
            fixed: false, //不固定
            maxmin: true, //开启最大化最小化按钮
            area: [w + 'px', h + 'px'],
            content: url
        });
    }
}

// 刷新当前窗口
function refresh(url) {
    // 有隐藏时frame需要增加
    if (url){
        window.location.href = url;
    }else{
        window.location.reload();
    }
}

function success(msg, timeout, callback) {
    parent.layer.msg(msg, {icon: 1,time: timeout});
    eval(callback);
}

function error(msg, timeout, close) {
    parent.layer.msg(msg, {time: timeout});
    if (close === 'on'){
        parent.layer.close(index);
    }
}

function jumpUrl(url, close) {
    if (url !== '') {
        refresh(url);
    } else {
        if (close === 'on'){
            refresh();
            setTimeout(function () {
                parent.layer.close(index);
            }, 1000)
        }
    }
}

function clearNoNum(obj, min, max){
    obj.value = obj.value.replace(/[^\d.]/g,"");
    obj.value = obj.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
    obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    if (min > 0){
        if (obj.value < min){
            obj.value = min;
        }
    }
    if (max > 0){
        if (obj.value > max){
            obj.value = max;
        }
    }
}
