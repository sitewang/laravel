//当浏览器窗口大小改变时重载网页
/*window.onresize=function(){
    window.location.reload();
}*/

//页面加载时给img和a标签添加draggable属性
(function () {
    $('img').attr('draggable', 'false');
    $('a').attr('draggable', 'false');
})();

//设置Cookie
function setCookie(name, value, time) {
    let strsec = getsec(time);
    let exp = new Date();
    exp.setTime(exp.getTime() + strsec * 1);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}
function getsec(str) {
    let str1 = str.substring(1, str.length) * 1;
    let str2 = str.substring(0, 1);
    if (str2 == "s") {
        return str1 * 1000;
    } else if (str2 == "h") {
        return str1 * 60 * 60 * 1000;
    } else if (str2 == "d") {
        return str1 * 24 * 60 * 60 * 1000;
    }
}

//获取Cookie
function getCookie(name) {
    let arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg)) {
        return unescape(arr[2]);
    } else {
        return null;
    }
}

let checkAll=document.getElementsByClassName('input-control');
//全选
function select(){
	for(let $i=0;$i<checkAll.length;$i++){
		checkAll[$i].checked=true;
	}
}
//反选
function reverse(){
	for(let $i=0;$i<checkAll.length;$i++){
		if(checkAll[$i].checked){
			checkAll[$i].checked=false;
		}else{
			checkAll[$i].checked=true;
		}
	}
}
//全不选
function noselect(){
	for(let $i=0;$i<checkAll.length;$i++){
		checkAll[$i].checked=false;
	}
}

function error(msg, time) {
    layer.msg('' + msg + '' , {time: time ,icon: 0});
    setTimeout(function () {
        $('#shade').remove();
    }, time ? time : 3000);

}

function success(msg, time, url) {
    layer.msg(msg , {time: time ,icon: 1});
    setTimeout(function () {
        $('#shade').remove();
        eval(url);
    }, time ? time : 3000);
}

function jumpUrl(url) {
    if (url) {
        location.href = url;
    } else {
        history.back(-1);
    }
}

function submit_form(form, submit_url){
    $('#' + form).attr('autocomplete', 'off').submit(function () {
        $.post(submit_url, $(this).serialize(), function (result) {
            $('body').append(shade());
            eval(result);
        });
        return false;
    });
}

function deletes(url,id) {
    layer.confirm('确定要删除吗？', {
        btn: ['确定','取消'] //按钮
    }, function(){
        $.post(url,{id:id},function (result) {
            $('body').append(shade());
            eval(result);
        })
    });
}

// handle js function
function handle(obj,id,is_confirm){
    // is confirm
    if (typeof(is_confirm) === 'undefined'){
        is_confirm = true;
    }

    let url = $(obj).attr('url');
    if (is_confirm === true){
        layer.confirm('确定要' + $(obj).html() + '吗？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.post(url,{id:id},function (result) {
                $('body').append(shade());
                eval(result);
            })
        });
    }else{
        $.post(url,{id:id},function (result) {
            $('body').append(shade());
            eval(result);
        });
    }

}

function shade() {
    let str = '<div id="shade">';
        str += '<div class="layui-layer-shade" id="layui-layer-shade16" style="z-index:10000; background-color:#000; opacity:0.3; filter:alpha(opacity=30);"></div>'
        str += '</div>'
    return str;
}

//IE6-9禁止用户选中文本
/*document.body.onselectstart = document.body.ondrag = function () {
    return false;
};*/

//启用工具提示
// $('[data-toggle="tooltip"]').tooltip();


//禁止右键菜单
/*window.oncontextmenu = function(){
	return false;
};*/

/*自定义右键菜单*/
/*(function () {
    let oMenu = document.getElementById("rightClickMenu");
    let aLi = oMenu.getElementsByTagName("li");
	//加载后隐藏自定义右键菜单
	//oMenu.style.display = "none";
    //菜单鼠标移入/移出样式
    for (i = 0; i < aLi.length; i++) {
        //鼠标移入样式
        aLi[i].onmouseover = function () {
            $(this).addClass('rightClickMenuActive');
			//this.className = "rightClickMenuActive";
        };
        //鼠标移出样式
        aLi[i].onmouseout = function () {
            $(this).removeClass('rightClickMenuActive');
			//this.className = "";
        };
    }
    //自定义菜单
    document.oncontextmenu = function (event) {
		$(oMenu).fadeOut(0);
        let event = event || window.event;
        let style = oMenu.style;
        $(oMenu).fadeIn(300);
		//style.display = "block";
        style.top = event.clientY + "px";
        style.left = event.clientX + "px";
        return false;
    };
    //页面点击后自定义菜单消失
    document.onclick = function () {
        $(oMenu).fadeOut(100);
		//oMenu.style.display = "none"
    }
})();*/

/*禁止键盘操作*/
/*document.onkeydown=function(event){
	let e = event || window.event || arguments.callee.caller.arguments[0];
	if((e.keyCode === 123) || (e.ctrlKey) || (e.ctrlKey) && (e.keyCode === 85)){
		return false;
	}
}; */
