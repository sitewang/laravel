<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'Home'], function (){
    Route::get('/', 'IndexController@index')->name('index');
    Route::get('/links', 'IndexController@links')->name('links');
    Route::get('/tags', 'IndexController@tags')->name('tags');
    Route::get('/category', 'IndexController@category')->name('category');
    Route::get('/details', 'ArticleController@details')->name('details');
});
Route::group(['namespace' => 'Admin'], function (){
    Route::get('admin/login', 'LoginController@index')->name('admin.login');
    Route::match(['get', 'post'],'admin/login-into', 'LoginController@into')->name('admin.loginInto');
    Route::get('admin/login-out', 'LoginController@out')->name('admin.loginOut');

    Route::get('admin/', 'IndexController@index')->name('admin');
    Route::get('admin/index', 'IndexController@index')->name('admin');

    // article
    Route::get('admin/article', 'ArticleController@index')->name('admin.article');
    Route::get('admin/article-insert', 'ArticleController@insert')->name('admin.insertArticle');
    Route::get('admin/article-update', 'ArticleController@update')->name('admin.updateArticle');
    Route::post('admin/article-save', 'ArticleController@save')->name('admin.saveArticle');
    Route::match(['get', 'post'],'admin/article-delete', 'ArticleController@delete')->name('admin.deleteArticle');

    // notice
    Route::get('admin/notice', 'NoticeController@index')->name('admin.notice');
    Route::get('admin/notice-insert', 'NoticeController@insert')->name('admin.insertNotice');
    Route::get('admin/notice-update', 'NoticeController@update')->name('admin.updateNotice');
    Route::post('admin/notice-save', 'NoticeController@save')->name('admin.saveNotice');
    Route::match(['get', 'post'],'admin/notice-delete', 'NoticeController@delete')->name('admin.deleteNotice');

    // comment
    Route::get('admin/comment', 'CommentController@index')->name('admin.comment');
    Route::get('admin/comment-update', 'CommentController@update')->name('admin.updateComment');
    Route::match(['get', 'post'],'admin/comment-delete', 'CommentController@delete')->name('admin.deleteComment');

    // category
    Route::get('admin/category', 'CategoryController@index')->name('admin.category');
    Route::get('admin/category-insert', 'CategoryController@insert')->name('admin.insertCategory');
    Route::get('admin/category-update', 'CategoryController@update')->name('admin.updateCategory');
    Route::post('admin/category-save', 'CategoryController@save')->name('admin.saveCategory');
    Route::match(['get', 'post'],'admin/category-delete', 'CategoryController@delete')->name('admin.deleteCategory');

    // links
    Route::get('admin/links', 'LinksController@index')->name('admin.links');
    Route::get('admin/links-insert', 'LinksController@insert')->name('admin.insertLinks');
    Route::get('admin/links-update', 'LinksController@update')->name('admin.updateLinks');
    Route::post('admin/links-save', 'LinksController@save')->name('admin.saveLinks');
    Route::match(['get', 'post'],'admin/links-delete', 'LinksController@delete')->name('admin.deleteLinks');

    // user
    Route::get('admin/user', 'UserController@index')->name('admin.user');
    Route::get('admin/user-insert', 'UserController@insert')->name('admin.insertUser');
    Route::get('admin/user-update', 'UserController@update')->name('admin.updateUser');
    Route::post('admin/user-save', 'UserController@save')->name('admin.saveUser');
    Route::match(['get', 'post'],'admin/user-audit', 'UserController@audit')->name('admin.auditUser');
    Route::match(['get', 'post'],'admin/user-delete', 'UserController@delete')->name('admin.deleteUser');

    // setting
    Route::get('admin/site-setting', 'SettingController@site')->name('admin.siteSetting');
    Route::post('admin/save-setting', 'SettingController@save')->name('admin.saveSetting');
});
