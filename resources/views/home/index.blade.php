@extends('layouts.home')
@section('content')
<section class="container">
  <div class="content-wrap">
    <div class="content">
      <div class="jumbotron">
        <h1>欢迎访问异清轩博客</h1>
        <p>在这里可以看到前端技术，后端程序，网站内容管理系统等文章，还有我的程序人生！</p>
      </div>
      <div id="focusslide" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#focusslide" data-slide-to="0" class="active"></li>
          <li data-target="#focusslide" data-slide-to="1"></li>
          <li data-target="#focusslide" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <a href="" target="_blank">
              <img src="/home/images/banner/banner_01.jpg" alt="" class="img-responsive">
            </a>
            <!--<div class="carousel-caption"> </div>-->
          </div>
          <div class="item">
            <a href="" target="_blank">
              <img src="/home/images/banner/banner_02.jpg" alt="" class="img-responsive">
            </a>
            <!--<div class="carousel-caption"> </div>-->
          </div>
          <div class="item">
            <a href="" target="_blank">
              <img src="/home/images/banner/banner_03.jpg" alt="" class="img-responsive">
            </a>
            <!--<div class="carousel-caption"> </div>-->
          </div>
        </div>
        <a class="left carousel-control" href="#focusslide" role="button" data-slide="prev" rel="nofollow">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">上一个</span>
        </a>
        <a class="right carousel-control" href="#focusslide" role="button" data-slide="next" rel="nofollow">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">下一个</span>
        </a>
      </div>
      <article class="excerpt-minic excerpt-minic-index">
        <p class="note">【公告】：一次我下载几部电影，发现如果同时下载多部要等上几个小时</p>
      </article>
      @if (!empty($article_list->list))
      @foreach ($article_list->list as $items)
      <article class="excerpt excerpt-1">
        <header>
          <h2><a href="{{route('details', ['article_id'=>$items->article_id])}}" title="">{{$items->title}}</a></h2>
        </header>
        <p class="meta">
          <time class="views"><a class="red" href="">作者：{{$items->nickname}}</a></time>
          <time class="time">时间：{{$items->create_time}}</time>
          <time class="time"><a class="red" href="">分类：{{$items->cate_name}}</a></time>
          <span class="views">字数： {{$items->text_number}}</span>
        </p>
        <hr>
        <div class="note">{!!$items->contents!!}</div>
        <!-- <p class="show_more"><a href="javascript:;">阅读剩余部分</a></p> -->
      </article>
      @endforeach
      @endif
      <nav class="pagination">
        {!! $article_list->page !!}
      </nav>
    </div>
  </div>
  <aside class="sidebar">
    <div class="fixed">
      <div class="widget widget-tabs">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#notice" aria-controls="notice" role="tab" data-toggle="tab">网站公告</a></li>
          <li role="presentation"><a href="#centre" aria-controls="centre" role="tab" data-toggle="tab">会员中心</a></li>
          <li role="presentation"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">联系站长</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane notice active" id="notice">
            <ul>
              <li>
                <time datetime="2016-01-04">01-04</time>
                <a href="" target="_blank">欢迎访问异清轩博客</a></li>
              <li>
                <time datetime="2016-01-04">01-04</time>
                <a target="_blank" href="">在这里可以看到前端技术，后端程序，网站内容管理系统等文章，还有我的程序人生！</a></li>
              <li>
                <time datetime="2016-01-04">01-04</time>
                <a target="_blank" href="">在这个小工具中最多可以调用五条</a></li>
            </ul>
          </div>
          <div role="tabpanel" class="tab-pane centre" id="centre">
            <h4>需要登录才能进入会员中心</h4>
            <p> <a data-toggle="modal" data-target="#loginModal" class="btn btn-primary">立即登录</a> <a href="javascript:;" class="btn btn-default">现在注册</a> </p>
          </div>
          <div role="tabpanel" class="tab-pane contact" id="contact">
            <h2>Email:<br />
              <a href="mailto:admin@ylsat.com" data-toggle="tooltip" data-placement="bottom" title="admin@ylsat.com">admin@ylsat.com</a></h2>
          </div>
        </div>
      </div>
      <div class="widget widget_search">
        <form class="navbar-form" action="/Search" method="post">
          <div class="input-group">
            <input type="text" name="keyword" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
            <span class="input-group-btn">
            <button class="btn btn-default btn-search" name="search" type="submit">搜索</button>
            </span>
          </div>
        </form>
      </div>
    </div>
    <div class="widget widget_sentence">
      <h3>标签云</h3>
      <div class="widget-sentence-content">
        <ul class="plinks ptags">
          @foreach ($tags_list as $items)
          <li><a href="{{route('tags', ['tags_id'=>$items->tags_id])}}" title="{{$items->name}}">{{$items->name}}({{$items->looks}})</a></li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="widget widget_hot">
      <h3>热门文章</h3>
      <ul>
        @foreach ($hot_article as $items)
        <li>
          <a href="{{route('details', ['article_id'=>$items->article_id])}}">
            <span class="text">{{$items->title}}</span>
            <span class="muted"><i class="glyphicon glyphicon-time"></i>{{$items->create_time}}</span>
            <span class="muted"><i class="glyphicon glyphicon-eye-open"></i>{{$items->looks}}</span>
          </a>
        </li>
        @endforeach
      </ul>
    </div>
    <div class="widget widget_hot">
      <h3>友情链接</h3>
      <ul>
        @foreach ($links_list as $items)
        <li>
          <a href="{{$items->url}}">
            <span class="text">
              <img style="vertical-align: middle;" src="{{$items->imgurl}}" alt="LearnKu" height="32">
              {{$items->title}}
            </span>
          </a>
        </li>
        @endforeach
      </ul>
    </div>
  </aside>
</section>
@stop
