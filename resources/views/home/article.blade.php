@extends('layouts.home')
@section('content')
<section class="container">
  <div class="content-wrap">
    <div class="content">
      <article class="excerpt-minic excerpt-minic-index">
        <p class="note">
          @if (!empty($cate_name))
          <a href="{{route('index')}}">首页</a> / 分类 / {{$cate_name}}
          @endif
          @if (!empty($tags_name))
          <a href="{{route('index')}}">首页</a> / 标签 / {{$tags_name}}
          @endif
        </p>
      </article>
      @if (!empty($article_list->list))
      @foreach ($article_list->list as $items)
      <article class="excerpt excerpt-1">
        <header>
          <h2><a href="{{route('details', ['article_id'=>$items->article_id])}}" title="">{{$items->title}}</a></h2>
        </header>
        <p class="meta">
          <time class="views"><a class="red" href="">作者：{{$items->nickname}}</a></time>
          <time class="time">时间：{{$items->create_time}}</time>
          <time class="time"><a class="red" href="">分类：{{$items->cate_name}}</a></time>
          <span class="views">字数： {{$items->text_number}}</span>
        </p>
        <hr>
        <div class="note">{!!$items->contents!!}</div>
        <!-- <p class="show_more"><a href="javascript:;">阅读剩余部分</a></p> -->
      </article>
      @endforeach
      @endif
      <nav class="pagination">
        {{$article_list->list->appends(['cate_id'=>$cate_id,'tags_id'=>$tags_id])->links('paginate.default')}}
      </nav>
    </div>
  </div>
  <aside class="sidebar">
    <div class="fixed">
      <div class="widget widget_search">
        <form class="navbar-form" action="/Search" method="post">
          <div class="input-group">
            <input type="text" name="keyword" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
            <span class="input-group-btn">
            <button class="btn btn-default btn-search" name="search" type="submit">搜索</button>
            </span> </div>
        </form>
      </div>
    </div>
    <div class="widget widget_sentence">
      <h3>标签云</h3>
      <div class="widget-sentence-content">
        <ul class="plinks ptags">
          @foreach ($tags_list as $items)
          <li><a href="{{route('tags', ['tags_id'=>$items->tags_id])}}" title="{{$items->name}}">{{$items->name}}({{$items->looks}})</a></li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="widget widget_hot">
      <h3>热门文章</h3>
      <ul>
        @foreach ($hot_article as $items)
        <li>
          <a href="{{route('details', ['article_id'=>$items->article_id])}}">
            <span class="text">{{$items->title}}</span>
            <span class="muted"><i class="glyphicon glyphicon-time"></i>{{$items->create_time}}</span>
            <span class="muted"><i class="glyphicon glyphicon-eye-open"></i>{{$items->looks}}</span>
          </a>
        </li>
        @endforeach
      </ul>
    </div>
    <div class="widget widget_hot">
      <h3>友情链接</h3>
      <ul>
        @foreach ($links_list as $items)
        <li>
          <a href="{{$items->url}}">
            <span class="text">
              <img style="vertical-align: middle;" src="{{$items->imgurl}}" alt="LearnKu" height="32">
              {{$items->title}}
            </span>
          </a>
        </li>
        @endforeach
      </ul>
    </div>
  </aside>
</section>
@stop
