@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.saveSetting')}}" method="post" target="hide_form" autocomplete="off" draggable="false">
        <div class="col-md-9">
            <h1 class="page-header">常规设置</h1>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>站点标题</span></h2>
                <div class="add-article-box-content">
                    <input type="text" name="title" class="form-control" placeholder="请输入站点标题" value="@if(!empty($site->title)){{$site->title}}@endif" required autofocus autocomplete="off">
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>站点地址（URL）</span></h2>
                <div class="add-article-box-content">
                    <input type="text" name="url" class="form-control" placeholder="在此处输入站点地址（URL）" value="@if(!empty($site->url)){{$site->url}}@endif" required autocomplete="off">
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>站点关键字</span></h2>
                <div class="add-article-box-content">
                    <textarea class="form-control" name="keywords" autocomplete="off">@if(!empty($site->keywords)){{$site->keywords}}@endif</textarea>
                    <span class="prompt-text">关键字会出现在网页的keywords属性中。</span>
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>站点描述</span></h2>
                <div class="add-article-box-content">
                    <textarea class="form-control" name="describe" rows="4" autocomplete="off">@if(!empty($site->describe)){{$site->describe}}@endif</textarea>
                    <span class="prompt-text">描述会出现在网页的description属性中。</span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <h1 class="page-header">站点</h1>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>电子邮件地址</span></h2>
                <div class="add-article-box-content">
                    <input type="email" name="email" class="form-control" placeholder="在此处输入邮箱" value="@if(!empty($site->email)){{$site->email}}@endif" autocomplete="off" />
                    <span class="prompt-text">这个电子邮件地址仅为了管理方便而填写</span>
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>ICP备案号</span></h2>
                <div class="add-article-box-content">
                    <input type="text" name="icp" class="form-control" placeholder="在此处输入备案号" value="@if(!empty($site->icp)){{$site->icp}}@endif" autocomplete="off" />
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>版权所属</span></h2>
                <div class="add-article-box-content">
                    <input type="text" name="copyright" class="form-control" placeholder="在此处输入备案号" value="@if(!empty($site->copyright)){{$site->copyright}}@endif" autocomplete="off" />
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>登录超时</span></h2>
                <div class="add-article-box-content">
                    <input type="number" name="cookie" class="form-control" placeholder="在此处输入超时时间(s)" value="@if(!empty($site->cookie)){{$site->cookie}}@endif" required autocomplete="off" />
                    <span class="prompt-text">单位(秒),超时将强制退出</span>
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>保存</span></h2>
                <div class="add-article-box-content"><span class="prompt-text">请确定您对所有选项所做的更改</span></div>
                <div class="add-article-box-footer">
                    {{ csrf_field() }}
                    <button class="btn btn-primary" type="submit">更新</button>
                </div>
            </div>
        </div>
    </form>
@stop
