@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.deleteLinks')}}" method="post" target="hide_form">
        <div class="col-md-12">
            <h1 class="page-header">操作</h1>
            <ol class="breadcrumb">
                <li>
                    {!! u('admin.insertLinks', '', '添加链接') !!}
                </li>
            </ol>
            <h1 class="page-header">管理 <span class="badge">{{$links_list->count}}</span></h1>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th><span class="glyphicon glyphicon-th-large"></span> <span class="visible-lg">选择</span></th>
                        <th><span class="glyphicon glyphicon-bookmark"></span> <span class="visible-lg">名称</span></th>
                        <th><span class="glyphicon glyphicon-link"></span> <span class="visible-lg">URL</span></th>
                        <th><span class="glyphicon glyphicon-pencil"></span> <span class="visible-lg">操作</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($links_list->list as $items)
                    <tr>
                        <td><input type="checkbox" class="input-control" name="links_is[]" value="{{$items->links_id}}" /></td>
                        <td class="article-title">{{$items->title}}</td>
                        <td>{{$items->url}}</td>
                        <td>
                            {!! u('admin.updateLinks', ['links_id'=>$items->links_id], '修改') !!}
                            {!! u('admin.deleteLinks', ['links_id'=>$items->links_id], '删除', 'act') !!}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="message_footer">
                <nav>
                    <div class="btn-toolbar operation" role="toolbar">
                        <div class="btn-group" role="group">
                            <a class="btn btn-default" onClick="select()">全选</a>
                            <a class="btn btn-default" onClick="reverse()">反选</a>
                            <a class="btn btn-default" onClick="noselect()">不选</a>
                        </div>
                        <div class="btn-group" role="group">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="删除全部选中">删除</button>
                        </div>
                    </div>
                    <ul class="pagination pagenav">
                        {!! $links_list->page !!}
                    </ul>
                </nav>
            </footer>
        </div>
    </form>
@stop
