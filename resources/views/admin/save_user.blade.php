<body style="padding-top: 0">
<iframe id="hide_form" name="hide_form" style="display:none;"></iframe>
@include('layouts.admin_title')
<form action="{{route('admin.saveUser')}}" method="post" target="hide_form" style="background: #FFFFFF;">
    <div class="modal-body">
        <table class="table" style="margin-bottom:0;">
            <tbody>
            <tr>
                <td wdith="20%">用户名:</td>
                <td>
                    @if(!empty($oldData->account))
                        {{$oldData->account}}
                    @else
                        <input type="text" value="" class="form-control" name="username" maxlength="10" autocomplete="off" />
                    @endif
                </td>
            </tr>
            <tr>
                <td wdith="20%">昵称:</td>
                <td><input type="text" value="@if(!empty($oldData->nickname)){{$oldData->nickname}}@endif" class="form-control" name="nickname" maxlength="20" autocomplete="off" /></td>
            </tr>
            <tr>
                <td wdith="20%">电话:</td>
                <td><input type="text" value="@if(!empty($oldData->mobile)){{$oldData->mobile}}@endif" class="form-control" name="mobile" autocomplete="off" /></td>
            </tr>
            <tr>
                <td wdith="20%">邮箱:</td>
                <td><input type="text" value="@if(!empty($oldData->email)){{$oldData->email}}@endif" class="form-control" name="email" autocomplete="off" /></td>
            </tr>
            <tr>
                <td wdith="20%">账户密码:</td>
                <td><input type="text" class="form-control" name="password" maxlength="18" autocomplete="off" /></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="@if(!empty($oldData->user_id)){{$oldData->user_id}}@endif">
        <button type="submit" class="btn btn-primary">提交</button>
    </div>
</form>
@include('layouts.admin_footer')
