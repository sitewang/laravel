@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.deleteComment')}}" method="post" target="hide_form">
        <div class="col-md-12">
            <h1 class="page-header">管理 <span class="badge">{{$comment_list->count}}</span></h1>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th><span class="glyphicon glyphicon-th-large"></span> <span class="visible-lg">选择</span></th>
                        <th><span class="glyphicon glyphicon-file"></span> <span class="visible-lg">摘要</span></th>
                        <th><span class="glyphicon glyphicon-time"></span> <span class="visible-lg">日期</span></th>
                        <th><span class="glyphicon glyphicon-pencil"></span> <span class="visible-lg">操作</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($comment_list->list as $items)
                    <tr>
                        <td><input type="checkbox" class="input-control" name="comment_id[]" value="{{$items->comment_id}}" /></td>
                        <td class="article-title">{!! htmlspecialchars_decode($items->content) !!}</td>
                        <td>{{$items->create_time}}</td>
                        <td>
                            <a href="{{route('details', ['article_id'=>$items->article_id])}}" target="_blank">查看</a>
                            {!! u('admin.deleteComment', ['comment_id'=>$items->comment_id], '删除', 'act') !!}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="message_footer">
                <nav>
                    <div class="btn-toolbar operation" role="toolbar">
                        <div class="btn-group" role="group">
                            <a class="btn btn-default" onClick="select()">全选</a>
                            <a class="btn btn-default" onClick="reverse()">反选</a>
                            <a class="btn btn-default" onClick="noselect()">不选</a>
                        </div>
                        <div class="btn-group" role="group">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="删除全部选中">删除</button>
                        </div>
                    </div>
                    <ul class="pagination pagenav">
                        {!! $comment_list->page !!}
                    </ul>
                </nav>
            </footer>
        </div>
    </form>
@stop
