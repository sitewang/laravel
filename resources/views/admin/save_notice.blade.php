@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.saveNotice')}}" method="post" target="hide_form" class="add-article-form">
        <div class="col-md-9">
            <h1 class="page-header">撰写新公告</h1>
            <div class="form-group">
                <label for="article-title" class="sr-only">标题</label>
                <input type="text" id="article-title" name="title" class="form-control" placeholder="在此处输入标题" value="@if (!empty($oldData->title)) {!! $oldData->title !!} @endif" required autofocus autocomplete="off">
            </div>
            <div class="form-group">
                <label for="article-content" class="sr-only">内容</label>
                <textarea id="article-content" name="contents" class="form-control">
                    @if (!empty($oldData->contents)) {!! $oldData->contents !!} @endif
                </textarea>
            </div>
        </div>
        <div class="col-md-3">
            <h1 class="page-header">操作</h1>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>发布</span></h2>
                <div class="add-article-box-content">
                    <p><label>状态：</label><span class="article-status-display">未发布</span></p>
                    <p>
                        <label>公开度：</label>
                        <input type="radio" name="is_show" value="1" checked/>公开
                        <input type="radio" name="is_show" value="0" />草稿箱
                    </p>
                </div>
                <div class="add-article-box-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="notice_id" value="@if(!empty($oldData->notice_id)){{$oldData->notice_id}}@endif">
                    <button class="btn btn-primary" type="submit">发布</button>
                </div>
            </div>
        </div>
    </form>
@stop
