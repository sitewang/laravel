@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.saveCategory')}}" method="post" target="hide_form" autocomplete="off">
        <div class="col-md-12">
            <h1 class="page-header">添加</h1>
            <div class="form-group">
                <label for="category-name">栏目名称</label>
                <input type="text" id="category-name" name="name" class="form-control" placeholder="在此处输入栏目名称" value="@if(!empty($oldData->name)){{$oldData->name}}@endif" required autocomplete="off">
                <span class="prompt-text">这将是它在站点上显示的名字。</span>
            </div>
            <div class="form-group">
                <label for="category-alias">栏目别名</label>
                <input type="text" id="category-alias" name="alias" class="form-control" placeholder="在此处输入栏目别名" value="@if(!empty($oldData->alias)){{$oldData->alias}}@endif" required autocomplete="off">
                <span class="prompt-text">“别名”是在URL中使用的别称，它可以令URL更美观。通常使用小写，只能包含字母，数字和连字符（-）。</span>
            </div>
            <div class="form-group">
                <label for="category-fname">父节点</label>
                <select id="category-fname" class="form-control" name="fid">
                    <option value="0" @if(empty($oldData->pid)) selected @endif>无</option>
                    @foreach($category_list as $items)
                    <option value="{{$items->category_id}}" @if($items->is_check == 1) selected @endif>{{$items->name}}</option>
                    @endforeach
                </select>
                <span class="prompt-text">栏目是有层级关系的，您可以有一个“音乐”分类目录，在这个目录下可以有叫做“流行”和“古典”的子目录。</span>
            </div>
            <div class="form-group">
                <label for="category-keywords">关键字</label>
                <input type="text" id="category-keywords" name="keywords" class="form-control" placeholder="在此处输入栏目关键字" value="@if(!empty($oldData->keywords)){{$oldData->keywords}}@endif" autocomplete="off">
                <span class="prompt-text">关键字会出现在网页的keywords属性中。</span>
            </div>
            <div class="form-group">
                <label for="category-describe">描述</label>
                <textarea class="form-control" id="category-describe" name="describe" rows="4" autocomplete="off">
                    @if(!empty($oldData->describe)){{$oldData->describe}}@endif
                </textarea>
                <span class="prompt-text">描述会出现在网页的description属性中。</span>
            </div>
            {{ csrf_field() }}
            <input type="hidden" name="category_id" value="@if(!empty($oldData->category_id)){{$oldData->category_id}}@endif">
            <button class="btn btn-primary" type="submit">添加新栏目</button>
        </div>
    </form>
@stop
