@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.saveArticle')}}" method="post" target="hide_form" class="add-article-form">
        <div class="col-md-9">
            <h1 class="page-header">撰写新文章</h1>
            <div class="form-group">
                <label for="article-title" class="sr-only">标题</label>
                <input type="text" id="article-title" name="title" class="form-control" placeholder="在此处输入标题" value="@if (!empty($oldData->title)) {{$oldData->title}} @endif" required autofocus autocomplete="off">
            </div>
            <div class="form-group">
                <label for="article-content" class="sr-only">内容</label>
                <script id="article-content" name="contents" type="text/plain">
                    @if (!empty($oldData->contents)) {!! $oldData->contents !!} @endif
                </script>
            </div>
        </div>
        <div class="col-md-3">
            <h1 class="page-header">操作</h1>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>栏目</span></h2>
                <div class="add-article-box-content">
                    <ul class="category-list">
                        @foreach($category_list as $items)
                        <li>
                            <label>
                                <input name="cate_id" type="radio" value="{{$items->category_id}}" @if($items->is_check == 1) checked @endif >{{$items->name}}
                            </label>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>标签</span></h2>
                <div class="add-article-box-content">
                    <input type="text" class="form-control" placeholder="输入新标签" name="tags" autocomplete="off">
                    <span class="prompt-text">多个标签请用英文逗号|隔开</span>
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>发布</span></h2>
                <div class="add-article-box-content">
                    <p>
                        <label>状态：</label>
                        <span class="article-status-display">未发布</span>
                    </p>
                    <p>
                        <label>公开度：</label>
                        <input type="radio" name="is_show" value="1" checked/>公开
                        <input type="radio" name="is_show" value="0" />草稿箱
                    </p>
                 </div>
                <div class="add-article-box-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="article_id" value="@if(!empty($oldData->article_id)){{$oldData->article_id}}@endif">
                    <button class="btn btn-primary" type="submit">发布</button>
                </div>
            </div>
        </div>
    </form>
    <script src="/static/admin/lib/ueditor/ueditor.config.js"></script>
    <script src="/static/admin/lib/ueditor/ueditor.all.min.js"> </script>
    <script src="/static/admin/lib/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script id="uploadEditor" type="text/plain" style="display:none;"></script>
    <script type="text/javascript">
        let editor = UE.getEditor('article-content');
        window.onresize=function(){
            window.location.reload();
        }
        let _uploadEditor;
        $(function () {
            //重新实例化一个编辑器，防止在上面的editor编辑器中显示上传的图片或者文件
            _uploadEditor = UE.getEditor('uploadEditor');
            _uploadEditor.ready(function () {
                //设置编辑器不可用
                //_uploadEditor.setDisabled();
                //隐藏编辑器，因为不会用到这个编辑器实例，所以要隐藏
                _uploadEditor.hide();
                //侦听图片上传
                /*_uploadEditor.addListener('beforeInsertImage', function (t, arg) {
                    //将地址赋值给相应的input,只去第一张图片的路径
                    $("#pictureUpload").attr("value", arg[0].src);
                    //图片预览
                    //$("#imgPreview").attr("src", arg[0].src);
                })*/
                //侦听文件上传，取上传文件列表中第一个上传的文件的路径
                _uploadEditor.addListener('afterUpfile', function (t, arg) {
                    $("#fileUpload").attr("value", _uploadEditor.options.filePath + arg[0].url);
                })
            });
        });
        //弹出图片上传的对话框
        $('#upImage').click(function () {
            let myImage = _uploadEditor.getDialog("insertimage");
            myImage.open();
        });
        //弹出文件上传的对话框
        function upFiles() {
            let myFiles = _uploadEditor.getDialog("attachment");
            myFiles.open();
        }
    </script>
@stop
