@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h1 class="page-header">操作</h1>
        <ol class="breadcrumb">
            <li>
                {!! u('admin.insertCategory', '', '添加分类') !!}
            </li>
        </ol>
        <h1 class="page-header">管理 <span class="badge">{{count($category_list)}}</span></h1>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th><span class="glyphicon glyphicon-paperclip"></span> <span class="visible-lg">ID</span></th>
                    <th><span class="glyphicon glyphicon-file"></span> <span class="visible-lg">名称</span></th>
                    <th><span class="glyphicon glyphicon-list-alt"></span> <span class="visible-lg">别名</span></th>
                    <th><span class="glyphicon glyphicon-pushpin"></span> <span class="visible-lg">总数</span></th>
                    <th><span class="glyphicon glyphicon-pencil"></span> <span class="visible-lg">操作</span></th>
                </tr>
                </thead>
                <tbody>
                @foreach($category_list as $items)
                <tr>
                    <td>{{$items->category_id}}</td>
                    <td>{{$items->name}}</td>
                    <td>{{$items->alias}}</td>
                    <td>{{$items->article_count}}</td>
                    <td>
                        {!! u('admin.updateCategory', ['category_id'=>$items->category_id], '修改') !!}
                        {!! u('admin.deleteCategory', ['category_id'=>$items->category_id], '删除', 'act') !!}
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <span class="prompt-text"><strong>注：</strong>删除一个栏目也会删除栏目下的文章和子栏目,请谨慎删除!</span>
        </div>
    </div>
@stop
