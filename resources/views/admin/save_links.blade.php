@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.saveLinks')}}" method="post" target="hide_form" class="add-flink-form" autocomplete="off" draggable="false">
        <div class="col-md-12">
            <h1 class="page-header">修改友情链接</h1>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>名称</span></h2>
                <div class="add-article-box-content">
                    <input type="text" id="flink-name" name="title" class="form-control" value="@if(!empty($oldData->title)){{$oldData->title}}@endif" placeholder="在此处输入名称" required autofocus autocomplete="off">
                    <span class="prompt-text">例如：异清轩技术博客</span>
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>WEB地址</span></h2>
                <div class="add-article-box-content">
                    <input type="text" id="flink-url" name="url" class="form-control" value="@if(!empty($oldData->url)){{$oldData->url}}@endif" placeholder="在此处输入URL地址" required autocomplete="off">
                    <span class="prompt-text">例子：<code>http://www.myblogs.xyz/</code>——不要忘了<code>http://</code></span>
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>图像地址</span></h2>
                <div class="add-article-box-content">
                    <input type="text" id="flink-imgurl" name="imgurl" class="form-control" value="@if(!empty($oldData->imgurl)){{$oldData->imgurl}}@endif" placeholder="在此处输入图像地址" required autocomplete="off">
                    <span class="prompt-text">图像地址是可选的，可以为网站LOGO地址等</span>
                </div>
            </div>
            <div class="add-article-box">
                <h2 class="add-article-box-title"><span>描述</span></h2>
                <div class="add-article-box-content">
                    <textarea class="form-control" name="describe" autocomplete="off">
                        @if(!empty($oldData->describe)){{$oldData->describe}}@endif
                    </textarea>
                    <span class="prompt-text">描述是可选的手工创建的内容总结</span>
                </div>
            </div>
            {{ csrf_field() }}
            <input type="hidden" name="links_id" value="@if(!empty($oldData->links_id)){{$oldData->links_id}}@endif">
            <button class="btn btn-primary" type="submit">添加新栏目</button>
        </div>
    </form>
@stop
