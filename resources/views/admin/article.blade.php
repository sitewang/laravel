@extends('layouts.admin')
@section('content')
    <form action="{{route('admin.deleteArticle')}}" method="post" target="hide_form">
        <div class="col-md-12">
            <h1 class="page-header">操作</h1>
            <ol class="breadcrumb">
                <li>
                    {!! u('admin.insertArticle', '', '添加文章') !!}
                </li>
            </ol>
            <h1 class="page-header">管理 <span class="badge">{{$article_list->count}}</span></h1>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th><span class="glyphicon glyphicon-th-large"></span> <span class="visible-lg">选择</span></th>
                        <th><span class="glyphicon glyphicon-file"></span> <span class="visible-lg">标题</span></th>
                        <th><span class="glyphicon glyphicon-list"></span> <span class="visible-lg">栏目</span></th>
                        <th class="hidden-sm"><span class="glyphicon glyphicon-tag"></span> <span class="visible-lg">标签</span></th>
                        <th class="hidden-sm"><span class="glyphicon glyphicon-comment"></span> <span class="visible-lg">评论</span></th>
                        <th><span class="glyphicon glyphicon-time"></span> <span class="visible-lg">日期</span></th>
                        <th><span class="glyphicon glyphicon-pencil"></span> <span class="visible-lg">操作</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($article_list->list))
                        @foreach ($article_list->list as $items)
                        <tr>
                            <td><input type="checkbox" class="input-control" name="article_id[]" value="{{$items->article_id}}" /></td>
                            <td class="article-title">{{$items->title}}</td>
                            <td>{{$items->cate_name}}</td>
                            <td class="hidden-sm">{{$items->tags_name}}</td>
                            <td class="hidden-sm">{{$items->comment_count}}</td>
                            <td>{{$items->create_time}}</td>
                            <td>
                                {!! u('admin.updateArticle', ['article_id'=>$items->article_id], '修改') !!}
                                {!! u('admin.deleteArticle', ['article_id'=>$items->article_id], '删除', 'act') !!}
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <footer class="message_footer">
                <nav>
                    <div class="btn-toolbar operation" role="toolbar">
                        <div class="btn-group" role="group">
                            <a class="btn btn-default" onClick="select()">全选</a>
                            <a class="btn btn-default" onClick="reverse()">反选</a>
                            <a class="btn btn-default" onClick="noselect()">不选</a>
                        </div>
                        <div class="btn-group" role="group">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="删除全部选中">删除</button>
                        </div>
                    </div>
                    <ul class="pagination pagenav">
                        {!! $article_list->page !!}
                    </ul>
                </nav>
            </footer>
        </div>
    </form>
@stop
