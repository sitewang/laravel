@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h1 class="page-header">信息总览</h1>
        <div class="row placeholders">
            <div class="col-xs-6 col-sm-4 placeholder">
                <h4>文章</h4>
                <span class="text-muted">{{$count_info->article_count}} 条</span>
            </div>
            <div class="col-xs-6 col-sm-4 placeholder">
                <h4>评论</h4>
                <span class="text-muted">{{$count_info->comment_count}} 条</span>
            </div>
            <div class="col-xs-6 col-sm-4 placeholder">
                <h4>友链</h4>
                <span class="text-muted">{{$count_info->links_count}} 条</span>
            </div>
        </div>
        <h1 class="page-header">系统信息</h1>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <tbody>
                    <tr>
                        <td>管理员个数:</td>
                        <td>{{$count_info->user_count}} 人</td>
                        <td>服务器软件:</td>
                        <td>{{$system->server}}</td>
                    </tr>
                    <tr>
                        <td>浏览器:</td>
                        <td>{{$system->user_agent}}</td>
                        <td>PHP版本:</td>
                        <td>{{$system->php_version}}</td>
                    </tr>
                    <tr>
                        <td>操作系统:</td>
                        <td>{{$system->php_os}}</td>
                        <td>PHP运行方式:</td>
                        <td>{{$system->php_mod}}</td>
                    </tr>
                    <tr>
                        <td>服务器IP:</td>
                        <td>{{$system->server_ip}}</td>
                        <td>MYSQL版本:</td>
                        <td>{{$system->mysql}}</td>
                    </tr>
                    <tr>
                        <td>程序版本:</td>
                        <td class="version">{{$system->project_version}}</td>
                        <td>最大上传文件:</td>
                        <td>{{$system->up_max}}</td>
                    </tr>
                    <tr>
                        <td>程序编码:</td>
                        <td>{{$system->coding}}</td>
                        <td>当前时间:</td>
                        <td>{{$system->time}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <footer>
            <h1 class="page-header">程序信息</h1>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <tbody>
                    <tr>
                        <td><span style="display:inline-block; width:8em">版权所有</span>{{$site->copyright}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </footer>
    </div>
@stop

