@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h1 class="page-header">操作</h1>
        <ol class="breadcrumb">
            <li>
                {!! u('admin.insertUser', '', '添加用户', 'load', '', 430, 470) !!}
            </li>
        </ol>
        <h1 class="page-header">管理 <span class="badge">{{$user_list->count}}</span></h1>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th><span class="glyphicon glyphicon-th-large"></span> <span class="visible-lg">ID</span></th>
                    <th><span class="glyphicon glyphicon-user"></span> <span class="visible-lg">用户名</span></th>
                    <th><span class="glyphicon glyphicon-bookmark"></span> <span class="visible-lg">姓名</span></th>
                    <th><span class="glyphicon glyphicon-pushpin"></span> <span class="visible-lg">文章</span></th>
                    <th><span class="glyphicon glyphicon-time"></span> <span class="visible-lg">上次登录时间</span></th>
                    <th><span class="glyphicon glyphicon-pencil"></span> <span class="visible-lg">操作</span></th>
                </tr>
                </thead>
                <tbody>
                @if (!empty($user_list->list))
                    @foreach($user_list->list as $items)
                    <tr>
                        <td>{{$items->user_id}}</td>
                        <td>{{$items->username}}</td>
                        <td>{{$items->nickname}}</td>
                        <td>{{$items->article_count}}</td>
                        <td>{{$items->last_time}}</td>
                        <td>
                            {!! u('admin.updateUser', ['user_id'=>$items->user_id], '修改', 'load', '', 430, 470) !!}
                            @if ($items->user_id != 1)
                                @if ($items->status == 0)
                                    {!! u('admin.auditUser', ['user_id'=>$items->user_id], '禁用', 'act') !!}
                                @else
                                    {!! u('admin.auditUser', ['user_id'=>$items->user_id], '启用', 'act') !!}
                                @endif
                                {!! u('admin.deleteUser', ['user_id'=>$items->user_id], '删除', 'act') !!}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(function () {
            $("#main table tbody tr td a").click(function () {
                var name = $(this);
                var id = name.attr("rel"); //对应id
                if (name.attr("name") === "see") {
                    $.ajax({
                        type: "POST",
                        url: "/User/see",
                        data: "id=" + id,
                        cache: false, //不缓存此页面
                        success: function (data) {
                            var data = JSON.parse(data);
                            $('#truename').val(data.truename);
                            $('#username').val(data.username);
                            $('#usertel').val(data.usertel);
                            $('#userid').val(data.userid);
                            $('#seeUser').modal('show');
                        }
                    });
                } else if (name.attr("name") === "delete") {
                    if (window.confirm("此操作不可逆，是否确认？")) {
                        $.ajax({
                            type: "POST",
                            url: "/User/delete",
                            data: "id=" + id,
                            cache: false, //不缓存此页面
                            success: function (data) {
                                window.location.reload();
                            }
                        });
                    };
                };
            });
        });
    </script>
@stop
