<header class="header">
    <nav class="navbar navbar-default" id="navbar">
        <div class="container">
            <div class="header-topbar hidden-xs link-border">
                <a href="{{route('admin.login')}}" class="login">Hi,请登录</a>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar" aria-expanded="false"> <span class="sr-only"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <h1 class="logo hvr-bounce-in"><a href="{{route('index')}}">{{$site_data->title}}</a></h1>
            </div>
            <div class="collapse navbar-collapse" id="header-navbar">
                <ul class="nav navbar-nav navbar-right">
                    @if ($cate_id == 0)
                        <li class="hidden-index active">
                    @else
                        <li>
                            @endif
                            <a data-cont="首页" href="{{route('index')}}">首页</a>
                        </li>
                        @foreach ($category_list as $items)
                            @if ($cate_id == $items->category_id)
                                <li class="hidden-index active">
                            @else
                                <li>
                                    @endif
                                    <a href="{{route('category', ['cate_id'=>$items->category_id])}}">{{$items->name}}</a>
                                </li>
                                @endforeach
                </ul>
                <form class="navbar-form visible-xs" action="/Search" method="post">
                    <div class="input-group">
                        <input type="text" name="keyword" class="form-control" placeholder="请输入关键字" maxlength="20" autocomplete="off">
                        <span class="input-group-btn">
            <button class="btn btn-default btn-search" name="search" type="submit">搜索</button>
            </span> </div>
                </form>
            </div>
        </div>
    </nav>
</header>
