<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$site->title}}</title>
    <link rel="stylesheet" type="text/css" href="/static/admin/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/admin/css/style.css">
    <link rel="stylesheet" type="text/css" href="/static/admin/css/font-awesome.min.css">
    <link rel="apple-touch-icon-precomposed" href="/static/admin/images/icon/icon.png">
    <link rel="shortcut icon" href="/static/admin/images/icon/favicon.ico">
    <script src="/static/admin/js/jquery-2.1.4.min.js"></script>
    <script src="/static/admin/lib/layui/layui.js"></script>
    <script src="/static/admin/js/script.js"></script>
    <!--[if gte IE 9]>
    <script src="/static/admin/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/static/admin/js/html5shiv.min.js" type="text/javascript"></script>
    <script src="/static/admin/js/respond.min.js" type="text/javascript"></script>
    <script src="/static/admin/js/selectivizr-min.js" type="text/javascript"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script>window.location.href='upgrade-browser.html';</script>
    <![endif]-->
</head>
