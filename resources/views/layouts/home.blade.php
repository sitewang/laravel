<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$site_data->title}}</title>
    <link rel="stylesheet" type="text/css" href="/static/home/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/style.css">
    <link rel="stylesheet" type="text/css" href="/static/home/css/font-awesome.min.css">
    <link rel="apple-touch-icon-precomposed" href="/static/home/images/icon/icon.png">
    <link rel="shortcut icon" href="/static/home/images/icon/favicon.ico">
    <script src="/static/home/js/jquery-2.1.4.min.js"></script>
    <script src="/static/home/js/nprogress.js"></script>
    <script src="/static/home/js/jquery.lazyload.min.js"></script>
    <!--[if gte IE 9]>
    <script src="/static/home/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/static/home/js/html5shiv.min.js" type="text/javascript"></script>
    <script src="/static/home/js/respond.min.js" type="text/javascript"></script>
    <script src="/static/home/js/selectivizr-min.js" type="text/javascript"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script>window.location.href='upgrade-browser.html';</script>
    <![endif]-->
</head>
<body class="user-select">
@include('layouts._header')
@yield('content')
@include('layouts._footer')
</body>
<script src="/static/home/js/bootstrap.min.js"></script>
<script src="/static/home/js/jquery.ias.js"></script>
<script src="/static/home/js/scripts.js"></script>
</html>
