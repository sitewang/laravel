<aside class="col-sm-3 col-md-2 col-lg-2 sidebar">
    <ul class="nav nav-sidebar">
        <li @if ($routeControllerName == 'IndexController') class="active" @endif >
            <a href="{{route('admin')}}">报告</a>
        </li>
    </ul>
    <ul class="nav nav-sidebar">
        <li @if ($routeControllerName == 'ArticleController') class="active" @endif >
            <a href="{{route('admin.article')}}">文章管理</a>
        </li>
        <li @if ($routeControllerName == 'NoticeController') class="active" @endif >
            <a href="{{route('admin.notice')}}">公告管理</a>
        </li>
        <li @if ($routeControllerName == 'CommentController') class="active" @endif >
            <a href="{{route('admin.comment')}}">评论管理</a>
        </li>
    </ul>
    <ul class="nav nav-sidebar">
        <li @if ($routeControllerName == 'CategoryController') class="active" @endif >
            <a href="{{route('admin.category')}}">分类管理</a>
        </li>
        <li @if ($routeControllerName == 'LinksController') class="active" @endif >
            <a href="{{route('admin.links')}}">友情链接</a>
        </li>
    </ul>
    <ul class="nav nav-sidebar">
        <li @if ($routeControllerName == 'UserController') class="active" @endif >
            <a href="{{route('admin.user')}}">用户管理</a>
        </li>
        <li @if ($routeControllerName == 'SettingController') class="active" @endif >
            <a href="{{route('admin.siteSetting')}}">站点设置</a>
        </li>
    </ul>
</aside>
